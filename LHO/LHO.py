'''
LHO.py

Simulates the LHO interferometer via Finesse, and writes the results to a .txt file.
Purpose is to massively simulate the LHO interferometer.
python script meant to be run on a condor cluster.
This script will be called by a shell script as a single job as a part of a dag.

Craig Cahillane
April 6, 2020
'''

import os
import sys
import time
import numpy as np

import pykat

import argparse

def parse_args():
    parser = argparse.ArgumentParser()

    # Positional arguments
    parser.add_argument('dir_tag', type=str, help='Directory tag. Unique identifier of a single set of jobs specified by a dag. make_dag.py must control this, as it creates the corresponding /dag/{dir_tag} and /log/{dir_tag} directories as well as the /results/{dir_tag} directory.')
    
    # Optional arguments
    parser.add_argument('--meastype', type=str, default='frequency', help='Type of injection for measurement via fsig, default is "frequency"')

    parser.add_argument('--maxtem', type=str, default='4', help='Max')

    parser.add_argument('--input_power', type=str, default='33.3', help='Input power incident on back of power recycling mirror in watts')

    parser.add_argument('--imc_ifo_mode_match', type=str, default='1.0', help='Mode matching between IMC and IFO, default is 1.0')

    parser.add_argument('--roc_itmx', type=str, default='-1934', help='Radius of curvature of ITMX in meters')
    parser.add_argument('--roc_etmx', type=str, default='2245', help='Radius of curvature of ETMX in meters')
    parser.add_argument('--roc_itmy', type=str, default='-1934', help='Radius of curvature of ITMY in meters')
    parser.add_argument('--roc_etmy', type=str, default='2245', help='Radius of curvature of ETMY in meters')


    parser.add_argument('--flo', type=str, default='0.1', help='Low frequency range for simulation, default is 0.1 Hz')
    parser.add_argument('--fhi', type=str, default='10000', help='High frequency range for simulation, default is 10000 Hz')
    parser.add_argument('--fspacing', type=str, default='log', help='Frequency spacing of measurement, default is "log"')
    parser.add_argument('--fpoints', type=str, default=1000, help='Number of points to simulation, default is 1000')

    parser.add_argument('--job', type=str, help='If provided, prints the job number assigned by the dag file')
    parser.add_argument('--verbose', type=bool, default=True, help='Print helpful statements to terminal if True.  Default is False.')
    args = parser.parse_args()
    return args

def make_dir(directory):
    ''' Runs os.makedirs on input argument `directory` if `directory` does not exist'''
    if not os.path.exists(directory):
        print(f'Creating directory = {directory}')
        os.makedirs(directory)
    return


def main():
    # Start time
    start_time = time.time()

    # Get this directory
    file_dir = os.path.abspath(__file__)
    file_dir = file_dir.rsplit('/', maxsplit=1)[0] # grabs the directory where this file resides

    results_dir = '{}/results'.format(file_dir) 
    make_dir(results_dir) # Make the results directory

    # Parse arguments
    args = parse_args()

    # Get command line
    command_line = " ".join(sys.argv) # get entire command line used to call this script

    if args.verbose:
        print()
        print('command_line:')
        print('{command_line}')
        print()
        print(f'file_dir = {file_dir}')
        print(f'args.verbose = {args.verbose}')
        print(f'results_dir = {results_dir}')
        print()


    # Get datetime
    cur_datetime = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()) + ' UTC' 
    cur_datetime_underscore = cur_datetime.replace(' ', '_').replace(':', '_').replace('-', '_')
    

    # Set up results file
    file_name = f'LHO_simulation_meastype_{args.meastype}_job_{args.job}_time_{cur_datetime_underscore}.txt'
    full_file_name = f'{results_dir}/{file_name}'
    with open(full_file_name, 'w') as f:
        f.write(f'This file was written at {cur_datetime}\n')
        if args.job is not None:
            f.write(f'This file was written by dag job {args.job}\n')
        f.write(f'\n')
        f.write(f'The command line argument that wrote these results was\n')
        f.write(f'\n')
        f.write(f'{command_line}')
        f.write(f'\n')

    return 


if __name__ == '__main__':
    main()
