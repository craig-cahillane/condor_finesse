# LHO

Directory for simulating the LHO interferometer using the LIGO computer clusters via condor.

In this directory:
- `LHO.py` - contains the interferometer simulation via Finesse. Should be controlled by a load of arguments, and output a single text file full of the measurements of interest.  Post-processing scripts should be written to gather and plot all results.
- `run.sh` - calls the `LHO.py` script on the cluster machines.  Organizes the arguments set by `make_dag.py`
- `make_dag.py` - creates the .dag and .sub files which control how many jobs are created with what arguments.  These files should appear in the `/dag` directory.


