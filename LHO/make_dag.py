'''
make_dag.py

Makes the dag for the example finesse condor submit file.

Craig Cahillane
April 2, 2020
'''

import os
import time
import numpy as np

import argparse

def parse_args():
    parser = argparse.ArgumentParser()

    # Positional Arguments

    # Optional Arguments
    parser.add_argument('--dir_tag', type=str, default=time.strftime("%Y%m%d%H%M%S", time.gmtime()) + 'UTC', help='Directory tag.  Acts as unique identifier of a set of jobs.  Will create directories /log/{dir_tag}, /dag/{dir_tag}, and /results/{dir_tag}.  Default is the datetime this script was run in UTC.')
    parser.add_argument('--dagfile', type=str, default='LHO', help='dag file name, not including the .dag at the end.  Will also be used for .sub filename. Default is LHO')
    parser.add_argument('--verbose', type=bool, default=False, help='Print helpful statements to terminal if True.  Default is False.')
    args = parser.parse_args()
    return args

def make_dir(directory):
    ''' Runs os.makedirs on input argument `directory` if `directory` does not exist'''
    if not os.path.exists(directory):
        print(f'Creating directory = {directory}')
        os.makedirs(directory)
    return

def main():
    # Get the necessary arguments
    args = parse_args()

    # Prepare the dag file name
    file_dir = os.path.abspath(__file__)
    file_dir = file_dir.rsplit('/', maxsplit=1)[0]

    dag_dir = f'{file_dir}/dag/{args.dir_tag}'
    log_dir = f'{file_dir}/log/{args.dir_tag}'
    results_dir = f'{file_dir}/results/{args.dir_tag}'

    make_dir(dag_dir)
    make_dir(log_dir)
    make_dir(results_dir)

    if args.verbose:
        print()
        print('Dag made by this script will be placed in:')
        print(dag_dir)
        print()
        print('Log will be placed in:')
        print(log_dir)
        print()
        print('Results will be placed in:')
        print(results_dir)
        print()

    sub_file_name = f'{dag_dir}/{args.dagfile}.sub'
    dag_file_name = f'{dag_dir}/{args.dagfile}.dag'

    # Prepare the job argument defaults
    meastype = 'frequency'

    maxtem = 4
    input_power = 33.3 # W
    imc_ifo_mode_match = 1.0 

    roc_itmy = -1934.0
    roc_etmx =  2245.0
    roc_itmy = -1934.0
    roc_etmy =  2245.0

    flo = 0.1
    fhi = 10000
    fspacing = 'log'
    fpoints = 1000
    
    verbose = True

    # Change params
    if args.num_roc_itmy is not 1:
        roc_itmys = np.linspace(args.start_roc_itmy, args.end_roc_itmy, args.num_roc_itmy)

    # Write the subfile
    with open(sub_file_name, 'w') as sub:
        sub.write(f'Executable = {file_dir}/run.sh\n')
        sub.write(f'Universe = vanilla\n')
        sub.write(f'Arguments = $(dir_tag) $(job)\n')
        sub.write(f'Error =  {log_dir}/err.$(job)\n')
        sub.write(f'Output = {log_dir}/out.$(job)\n')
        sub.write(f'Log =    {log_dir}/log.$(job)\n')
        sub.write(f'Notification = never\n')
        sub.write(f'accounting_group = ligo.dev.o2.detchar.explore.test\n')
        sub.write(f'Queue 1')

    # Write the dagfile
    with open(dag_file_name, 'w') as dag:
        for job, nn in enumerate(numbers):
            dag.write(f'JOB {job} {sub_file_name}\n')
            dag.write(f'RETRY {job} 1\n')
            dag.write(f'VARS {job} macrojobnumber="{job}" dir_tag="{args.dir_tag}"\n')

    print('\033[92m')
    print(f'dag file successfully made at:')
    print(f'{dag_file_name}')
    print('\033[0m')
    
    return

if __name__ == '__main__':
    main()
