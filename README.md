# condor_finesse

Repository for using condor-based computer clusters for large-scale interferometer simulation via Finesse.

`condor_example` shows how to use the LIGO LDAS clusters
