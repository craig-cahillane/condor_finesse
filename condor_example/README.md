# condor_example

Contained in this directory is an example condor-based job launch.
There are three main components:

- `write_number.py`, the main python script we want to run on the cluster machine
- `run.sh`, a bash script wrapper which is actually run on the cluster machine.  All it does is call `write_number.py` with correct arguments
- `make_dag.py`, which makes the .dag file and .sub file.

The .dag file is a list of jobs we want to submit, with different arguments provided.
The .sub file is the job submission file, which tells condor what executable to run, which arguments to provide, and where to write errors, terminal outputs, and logs.

## What does this code do

 `write_number.py` creates a single file in the `results/` directory, with the name `<number>.txt`.  It writes a message reporting it's number, the time, and the job number which executed it.  It takes in the number as a positional argument, and the job number as an optional argument.  
You can run this script by hand using
```
python3 write_number.py 1001
```
It will create a file named `1001.txt` in the `results/` directory

`make_dag.py` will create a dag file which prepares to launch several jobs.  It takes in two arguments, `start_number` and `end_number`.

## Instructions

1) Run `make_dag.py` with your start_number, say it's 2000, and end_number, say it's 2100, like so:
```python
python3 make_dag.py 2000 2100
```
This will make a 100-job dag, in `/dag/write_number.dag`.  Each job will write one file between 2000.txt and 2100.txt in the `/results` folder.

2) Go to the `/dag` directory:
```bash
$ cd dag
```

3) Submit the 100-job dag to condor.  Include the `-f` or `--force` flag if you have run this dag before.
```bash
$ condor_submit_dag write_number.dag
```

4) Watch your jobs be submitted, idle, run, and complete with a one second update (change albert.einstein to your username).  
Press Ctrl-C to stop watching condor_q update in real time.
```bash
$ watch -n 1 "condor_q albert.einstein -nobatch"
```

5) Move to the results directory and check the results.
```bash
$ cd ../results
$ cat 2050.txt
```
