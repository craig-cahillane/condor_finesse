'''
make_dag.py

Makes the dag for the example finesse condor submit file.

Craig Cahillane
April 2, 2020
'''

import os
import time
import numpy as np

import argparse

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('start_number', type=int, help='Int.  Number to start making files at.')
    parser.add_argument('end_number', type=int, help='Int.  Number to end making files at.')
    parser.add_argument('--dagfile', type=str, default='write_number', help='dag file name, not including the .dag at the end.  Will also be used for .sub filename. default is write_number')
    parser.add_argument('--verbose', type=bool, default=False, help='Print helpful statements to terminal if True.  Default is False.')
    args = parser.parse_args()
    return args


def main():
    # Get the necessary arguments
    args = parse_args()

    # Prepare the dag file name
    file_dir = os.path.abspath(__file__)
    file_dir = file_dir.rsplit('/', maxsplit=1)[0]

    dag_dir = f'{file_dir}/dag'
    log_dir = f'{file_dir}/log'

    if args.verbose:
        print()
        print('Dag made by this script will be placed in:')
        print(dag_dir)
        print()
        print('Log files will be placed in:')
        print(log_dir)
        print()
    if not os.path.exists(dag_dir):
        os.makedirs(dag_dir)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    sub_file_name = f'{dag_dir}/{args.dagfile}.sub'
    dag_file_name = f'{dag_dir}/{args.dagfile}.dag'

    # Prepare the dag jobs arguments
    numbers = np.arange(args.start_number, args.end_number+1)

    # Write the subfile
    with open(sub_file_name, 'w') as sub:
        sub.write(f'Executable = {file_dir}/run.sh\n')
        sub.write(f'Universe = vanilla\n')
        sub.write(f'Arguments = $(macroargument0) $(macrojobnumber)\n')
        sub.write(f'Error =  {file_dir}/log/err.$(macrojobnumber)\n')
        sub.write(f'Output = {file_dir}/log/out.$(macrojobnumber)\n')
        sub.write(f'Log =    {file_dir}/log/log.$(macrojobnumber)\n')
        sub.write(f'Notification = never\n')
        sub.write(f'accounting_group = ligo.dev.o2.detchar.explore.test\n')
        sub.write(f'Queue 1')

    # Write the dagfile
    with open(dag_file_name, 'w') as dag:
        for job, nn in enumerate(numbers):
            dag.write(f'JOB {job} {sub_file_name}\n')
            dag.write(f'RETRY {job} 1\n')
            dag.write(f'VARS {job} macrojobnumber="{job}" macroargument0="{nn}"\n')

    print('\033[92m')
    print(f'dag file successfully made at:')
    print(f'{dag_file_name}')
    print('\033[0m')
    
    return

if __name__ == '__main__':
    main()
