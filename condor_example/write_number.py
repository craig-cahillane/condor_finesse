'''
write_number.py

Writes a number to a file.
python script meant to be run on a condor cluster.
This script will be called by a shell script as a single job as a part of a dag.

Craig Cahillane
April 2, 2020
'''

import os
import time
import numpy as np

# import pykat

import argparse

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('number', type=str, help='Number to be printed to file.')
    parser.add_argument('--job', type=str, help='If provided, prints the job number assigned by the dag file')
    parser.add_argument('--verbose', type=bool, default=False, help='Print helpful statements to terminal if True.  Default is False.')
    args = parser.parse_args()
    return args

def main():
    file_dir = os.path.abspath(__file__)
    file_dir = file_dir.rsplit('/', maxsplit=1)[0]

    args = parse_args()
    
    if args.verbose:
        print(f'file_dir = {file_dir}')
        print(f'args.number = {args.number}')
        print(f'args.verbose = {args.verbose}')
        print()

    results_dir = '{}/results'.format(file_dir) 
    if args.verbose:
        print()
        print('Results made by this script will be placed in:')
        print(results_dir)
        print()
    if not os.path.exists(results_dir):
        print()
        print('Making this directory')
        os.makedirs(results_dir)

    cur_datetime = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()) + ' UTC' 

    file_name = f'{args.number}.txt'
    full_file_name = f'{results_dir}/{file_name}'
    with open(full_file_name, 'w+') as f:
        f.write(f'Hello\n')
        f.write(f'My number is {args.number}\n')
        f.write(f'This file was written at {cur_datetime}\n')
        if args.job is not None:
            f.write(f'This file was written by dag job {args.job}')

    return 


if __name__ == '__main__':
    main()
